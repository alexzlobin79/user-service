package com.carx;

import com.carx.dto.common.UserInfoDto;
import com.carx.dto.request.ActivityUserInfoDto;
import com.carx.entity.ActivityUserInfo;
import com.carx.entity.UserInfo;
import com.carx.exception.mapping.MappingPayLoadException;
import com.carx.utils.EntityDtoMapper;
import com.fasterxml.jackson.databind.JsonNode;
import com.vladmihalcea.hibernate.type.json.internal.JacksonUtil;
import org.junit.Test;
import org.springframework.test.context.ActiveProfiles;

import java.io.IOException;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

@ActiveProfiles("dev")
public class EntityDtoMapper_UnitTest {
    private final EntityDtoMapper entityDtoMapper = new EntityDtoMapper();

    @Test
    public void whenConvertUserInfoEntityToUserInfoDto_thenCorrect() throws IOException {
        UserInfo userInfo = new UserInfo();
        userInfo.setId(Long.valueOf(1));
        userInfo.setMoney(100);
        userInfo.setUuid(UUID.randomUUID());
        userInfo.setCountry("USA");
        String payLoadJson = "{\"money\":100,\"country\":\"RU\",\"key1\":\"value1\",\"key2\":\"value2\"}";
        JsonNode jsonNode = JacksonUtil.toJsonNode(payLoadJson);
        userInfo.setPayload(jsonNode);
        UserInfoDto userInfoDto = entityDtoMapper.convertUserInfoToDto(userInfo);
        assertEquals(userInfo.getUuid(), userInfoDto.getUuid());
        assertEquals(userInfo.getPayload(), userInfoDto.getPayload());
    }

    @Test
    public void whenConvertUserInfoDtoToUserInfoEntity_thenCorrect() throws IOException {
        UserInfoDto userInfoDto = new UserInfoDto();
        userInfoDto.setUuid(UUID.randomUUID());
        String payLoadJson = "{\"money\":1000,\"country\":\"USA\",\"key1\":\"value1\",\"key2\":\"value2\"}";
        JsonNode jsonNode = JacksonUtil.toJsonNode(payLoadJson);
        userInfoDto.setPayload(jsonNode);
        UserInfo userInfo = entityDtoMapper.convertUserInfoDtoToEntity(userInfoDto);
        assertEquals(userInfoDto.getUuid(), userInfo.getUuid());
        assertEquals(userInfoDto.getPayload(), userInfo.getPayload());
        assertEquals("USA", userInfo.getCountry());
        assertEquals(1000, (long) userInfo.getMoney());
    }

    @Test
    public void whenConvertActivityUserInfoDtoToActivityUserInfoEntity_thenCorrect() {
        ActivityUserInfoDto activityUserInfoDto = new ActivityUserInfoDto(UUID.randomUUID(), 1001);
        ActivityUserInfo activityUserInfo = entityDtoMapper.convertActivityUserInfoDtoToEntity(activityUserInfoDto);
        assertEquals(activityUserInfo.getUuid(), activityUserInfoDto.getUuid());
        assertEquals(1001, (long) activityUserInfoDto.getActivity());
    }

    @Test(expected = IOException.class)
    public void whenConvertUserInfoDtoToUserInfoEntity_thenThrowIOException() throws IOException {
        UserInfoDto userInfoDto = new UserInfoDto();
        userInfoDto.setUuid(UUID.randomUUID());
        String payLoadJson = "{\"money\":\"1000k\",\"country\":\"USA\",\"key1\":\"value1\",\"key2\":\"value2\"}";
        JsonNode jsonNode = JacksonUtil.toJsonNode(payLoadJson);
        userInfoDto.setPayload(jsonNode);
        UserInfo userInfo = entityDtoMapper.convertUserInfoDtoToEntity(userInfoDto);
    }

    @Test(expected = MappingPayLoadException.class)
    public void whenConvertUserInfoDtoToUserInfoEntity_thenThrowMappingPayLoadExceptionCase1() throws IOException {
        UserInfoDto userInfoDto = new UserInfoDto();
        userInfoDto.setUuid(UUID.randomUUID());
        String payLoadJson = "{\"money\":null,\"country\":\"USA\",\"key1\":\"value1\",\"key2\":\"value2\"}";
        JsonNode jsonNode = JacksonUtil.toJsonNode(payLoadJson);
        userInfoDto.setPayload(jsonNode);
        UserInfo userInfo = entityDtoMapper.convertUserInfoDtoToEntity(userInfoDto);
    }

    @Test(expected = MappingPayLoadException.class)
    public void whenConvertUserInfoDtoToUserInfoEntity_thenThrowMappingPayLoadExceptionCase2() throws IOException {
        UserInfoDto userInfoDto = new UserInfoDto();
        userInfoDto.setUuid(UUID.randomUUID());
        String payLoadJson = "{\"money\":1000,\"country\":\"\",\"key1\":\"value1\",\"key2\":\"value2\"}";
        JsonNode jsonNode = JacksonUtil.toJsonNode(payLoadJson);
        userInfoDto.setPayload(jsonNode);
        UserInfo userInfo = entityDtoMapper.convertUserInfoDtoToEntity(userInfoDto);
    }
}
