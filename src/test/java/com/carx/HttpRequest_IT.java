package com.carx;

import com.carx.dto.common.UserInfoDto;
import com.carx.dto.request.ActivityUserInfoDto;
import com.fasterxml.jackson.databind.JsonNode;
import com.vladmihalcea.hibernate.type.json.internal.JacksonUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@SqlGroup({@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/sql/beforeTestRun.sql"), @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:/sql/afterTestRun.sql")})
public class HttpRequest_IT {
    private final String RESOURCE_URL = "http://localhost:";
    @LocalServerPort
    private int port;
    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    public void getUserInfoShouldReturnNotFoundStatus() throws Exception {
        ResponseEntity<UserInfoDto> response = testRestTemplate.getForEntity(getUrl("api/v1/data/user/123e4567-e89b-12d3-a456-556642440005"), UserInfoDto.class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void getUserInfoShouldReturnOkStatusAndResponseEqualsSavedToDb() throws Exception {
        ResponseEntity<UserInfoDto> response = testRestTemplate.getForEntity(getUrl("api/v1/data/user/123e4567-e89b-12d3-a456-556642440000"), UserInfoDto.class);
        assertEquals(response.getBody().getClass(), UserInfoDto.class);
        UserInfoDto actual = response.getBody();
        String payLoadJson = "{\"money\":10005,\"country\":\"US\",\"key1\":\"value1\",\"key2\":\"value2\"}";
        JsonNode jsonNodeExcpected = JacksonUtil.toJsonNode(payLoadJson);
        assertEquals("123e4567-e89b-12d3-a456-556642440000", actual.getUuid().toString());
        assertEquals(jsonNodeExcpected, actual.getPayload());
        assertEquals(UserInfoDto.class, response.getBody().getClass());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void addUserInfoShouldReturnOkStatus() throws Exception {
        ResponseEntity<UserInfoDto> response = testRestTemplate.getForEntity(getUrl("api/v1/data/user/123e4567-e89b-12d3-a456-556642440000"), UserInfoDto.class);
        assertEquals(response.getBody().getClass(), UserInfoDto.class);
        UserInfoDto actual = response.getBody();
        assertEquals("123e4567-e89b-12d3-a456-556642440000", actual.getUuid().toString());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void addActivityUserInfoShouldReturnOkStatus() throws Exception {
        ActivityUserInfoDto activityUserInfoDto = new ActivityUserInfoDto(UUID.randomUUID(), 105);
        ResponseEntity<Void> response = testRestTemplate.postForEntity(getUrl("api/v1/activity/user"), activityUserInfoDto, Void.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    private String getUrl(String urlApi) {
        return RESOURCE_URL + port + "/" + urlApi;
    }
}