package com.carx;

import com.carx.dto.common.UserInfoDto;
import com.carx.dto.request.ActivityUserInfoDto;
import com.carx.entity.UserInfo;
import com.carx.repository.ActivityUserInfoRepository;
import com.carx.repository.UserInfoRepository;
import com.carx.service.UserServiceImpl;
import com.carx.utils.EntityDtoMapper;
import com.fasterxml.jackson.databind.JsonNode;
import com.vladmihalcea.hibernate.type.json.internal.JacksonUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.ActiveProfiles;

import java.io.IOException;
import java.util.Optional;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
@ActiveProfiles("dev")
public class CorrectionUserService_UnitTest {
    @Mock
    UserInfoRepository userInfoRepository;
    @Mock
    ActivityUserInfoRepository activityUserInfoRepository;
    @Mock
    EntityDtoMapper entityDtoMapper;
    @InjectMocks
    private UserServiceImpl userService;

    @Test
    public void whenFindSynchData_ExcpectedUserInfoDto() {
        String payLoadJson = "{\"money\":100,\"country\":\"RU\",\"key1\":\"value1\",\"key2\":\"value2\"}";
        JsonNode jsonNode = JacksonUtil.toJsonNode(payLoadJson);
        UserInfo returnUserInfo = new UserInfo(UUID.fromString("123e4567-e89b-12d3-a456-556642440000"), 100, "RU", jsonNode);
        UserInfoDto returnUserInfoDto = new UserInfoDto(UUID.fromString("123e4567-e89b-12d3-a456-556642440000"), jsonNode);
        when(userInfoRepository.findByUuid(any(UUID.class))).thenReturn(returnUserInfo);
        when(entityDtoMapper.convertUserInfoToDto(any(UserInfo.class))).thenReturn(returnUserInfoDto);
        Optional<UserInfoDto> actual= userService.findSynchData(UUID.fromString("123e4567-e89b-12d3-a456-556642440000"));
        assertEquals(Optional.of(returnUserInfoDto),actual);
    }

    @Test
    public void shouldCallVoidMethodEntityDtoMapper1() throws IOException {
        String payLoadJson = "{\"money\":100,\"country\":\"RU\",\"key1\":\"value1\",\"key2\":\"value2\"}";
        JsonNode jsonNode = JacksonUtil.toJsonNode(payLoadJson);
        UserInfoDto savedUserInfoDto = new UserInfoDto(UUID.fromString("123e4567-e89b-12d3-a456-556642440000"), jsonNode);
        userService.saveSynchData(savedUserInfoDto);
        verify(entityDtoMapper, times(1)).convertUserInfoDtoToEntity(savedUserInfoDto);
    }

    @Test
    public void shouldCallVoidMethodEntityDtoMapper2() throws IOException {
        ActivityUserInfoDto savedActivityUserInfoDto = new ActivityUserInfoDto(UUID.fromString("123e4567-e89b-12d3-a456-556642440000"), 1000);
        userService.saveActivityData(savedActivityUserInfoDto);
        verify(entityDtoMapper, times(1)).convertActivityUserInfoDtoToEntity(savedActivityUserInfoDto);
    }
}



