package com.carx;

import com.carx.entity.ActivityUserInfo;
import com.carx.repository.ActivityUserInfoRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.sql.DataSource;
import java.time.LocalDateTime;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@AutoConfigureTestDatabase
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@DataJpaTest
public class ActivityUserInfoEntityRepository_IT {
    @Autowired
    private DataSource dataSource;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private EntityManager entityManager;
    @Autowired
    private ActivityUserInfoRepository activityUserInfoRepository;

    @Test
    public void injectedComponentsAreNotNull() {
        assertNotNull(dataSource);
        assertNotNull(jdbcTemplate);
        assertNotNull(entityManager);
        assertNotNull(activityUserInfoRepository);
    }

    @Test
    public void whenSaved_thenGetSaved() {
        ActivityUserInfo activityUserInfo = new ActivityUserInfo(UUID.fromString("123e4567-e89b-12d3-a456-556642440000"), 100, LocalDateTime.now());
        ActivityUserInfo activityUserInfoSaved = activityUserInfoRepository.save(activityUserInfo);
        assertEquals(activityUserInfo.getUuid(),activityUserInfoSaved.getUuid());
        assertEquals(activityUserInfo.getActivity(),activityUserInfoSaved.getActivity());
        assertEquals(activityUserInfo.getDateTime().getClass(),activityUserInfoSaved.getDateTime().getClass());
    }
}