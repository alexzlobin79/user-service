package com.carx;

import com.carx.entity.UserInfo;
import com.carx.repository.UserInfoRepository;
import com.fasterxml.jackson.databind.JsonNode;
import com.vladmihalcea.hibernate.type.json.internal.JacksonUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.sql.DataSource;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@AutoConfigureTestDatabase
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@DataJpaTest
public class UserInfoEntityRepository_IT {
    @Autowired
    private DataSource dataSource;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private EntityManager entityManager;
    @Autowired
    private UserInfoRepository userInfoRepository;

    @Test
    public void injectedComponentsAreNotNull() {
        assertNotNull(dataSource);
        assertNotNull(jdbcTemplate);
        assertNotNull(entityManager);
        assertNotNull(userInfoRepository);
    }

    @Test
    public void whenSaved_thenFindsByUuid() {
        String payLoadJson = "{\"money\":100,\"country\":\"RU\",\"key1\":\"value1\",\"key2\":\"value2\"}";
        JsonNode jsonNode = JacksonUtil.toJsonNode(payLoadJson);
        UserInfo userInfo = new UserInfo(UUID.fromString("123e4567-e89b-12d3-a456-556642440000"), 100, "RU", jsonNode);
        userInfoRepository.save(userInfo);
        UserInfo userInfoFound = userInfoRepository.findByUuid(UUID.fromString("123e4567-e89b-12d3-a456-556642440000"));
        assertEquals(userInfo, userInfoFound);
    }
}