package com.carx.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(description = "Информация по активности игрока")
@AllArgsConstructor
public class ActivityUserInfoDto {
    @Valid
    @NotNull(message = "UUID игрока отсутствует")
    @ApiModelProperty(value = "UUID игрока", example = "123e4567-e89b-12d3-a456-556642440000")
    private UUID uuid;
    @NotNull(message = "Активность игрока отсутствует")
    @ApiModelProperty(value = "Активность игрока", example = "100")
    private Integer activity;
}