package com.carx.dto.common;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.JsonNode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(description = "Информация для синхронизации игрока")
public class UserInfoDto {
    @Valid
    @NotNull(message = "UUID игрока отсутствует")
    @ApiModelProperty(value = "UUID игрока", example = "123e4567-e89b-12d3-a456-556642440000")
    private UUID uuid;
    @NotNull(message = "Полезная нагрузка игрока (JSON) не заполнена")
    @ApiModelProperty(value = "Полезная нагрузка игрока (JSON)", example = "<Your JSON>")
    private JsonNode payload;
}
