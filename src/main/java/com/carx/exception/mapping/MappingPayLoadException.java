package com.carx.exception.mapping;

import com.carx.exception.AppException;

public class MappingPayLoadException extends AppException {
    private static final long serialVersionUID = 4990959228756992926L;

    public MappingPayLoadException(String message, Throwable e) {
        super(message, e);
    }

    public MappingPayLoadException(String message) {
        super(message);
    }
}
