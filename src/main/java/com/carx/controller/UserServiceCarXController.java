package com.carx.controller;

import com.carx.dto.common.UserInfoDto;
import com.carx.dto.request.ActivityUserInfoDto;
import com.carx.exception.mapping.MappingPayLoadException;
import com.carx.service.IUserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.UUID;

@RequestMapping("/api/v1")
@AllArgsConstructor
//@Api(value = "UserServiceRestController  CarX-Tech")
@RestController
public class UserServiceCarXController {
    @Autowired
    private IUserService userService;

    @ApiOperation(value = "Save synchronization info user", response = void.class, tags = "saveSynchData")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Success|OK"), @ApiResponse(code = 400, message = "Bad request!Payload not valid")})
    @PostMapping(value = "/data/user", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public void saveSynchData(@Valid @RequestBody UserInfoDto userInfoDto) {
        try {
            userService.saveSynchData(userInfoDto);
        } catch (MappingPayLoadException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "payload not valid: " + ex.getMessage());
        }
    }

    @ApiOperation(value = "Find synchronization info user by uuid", response = UserInfoDto.class, tags = "findSynchData")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Success|OK"), @ApiResponse(code = 404, message = "Entity not found!")})
    @GetMapping(value = "/data/user/{uuid}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public UserInfoDto findSynchData(@PathVariable(name = "uuid", required = true) UUID uuid) {
        return userService.findSynchData(uuid).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "entity not found: " + uuid));
    }

    @ApiOperation(value = "Save activity info user", response = void.class, tags = "saveActivityData")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Success|OK")})
    @PostMapping(value = "/activity/user", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public void saveActivityData(@Valid @RequestBody ActivityUserInfoDto activityUserInfo) {
        userService.saveActivityData(activityUserInfo);
    }
}
