package com.carx.service;

import com.carx.dto.common.UserInfoDto;
import com.carx.dto.request.ActivityUserInfoDto;

import java.util.Optional;
import java.util.UUID;

public interface IUserService {
    void saveSynchData(final UserInfoDto userInfoDto);

    Optional<UserInfoDto> findSynchData(final UUID uuid);

    void saveActivityData(final ActivityUserInfoDto activityUserInfoDto);
}
