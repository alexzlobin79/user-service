package com.carx.service;

import com.carx.dto.common.UserInfoDto;
import com.carx.dto.request.ActivityUserInfoDto;
import com.carx.entity.ActivityUserInfo;
import com.carx.entity.UserInfo;
import com.carx.exception.mapping.MappingPayLoadException;
import com.carx.repository.ActivityUserInfoRepository;
import com.carx.repository.UserInfoRepository;
import com.carx.utils.EntityDtoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserServiceImpl implements IUserService {
    private final UserInfoRepository userInfoRepository;
    private final ActivityUserInfoRepository activityUserInfoRepository;
    private final EntityDtoMapper entityDtoMapper;

    @Autowired
    public UserServiceImpl(UserInfoRepository userInfoRepository, ActivityUserInfoRepository activityUserInfoRepository, EntityDtoMapper entityDtoMapper) {
        this.userInfoRepository = userInfoRepository;
        this.activityUserInfoRepository = activityUserInfoRepository;
        this.entityDtoMapper = entityDtoMapper;
    }

    @Override
    public void saveSynchData(final UserInfoDto userInfoDto) {
        UserInfo userInfo = null;
        try {
            userInfo = entityDtoMapper.convertUserInfoDtoToEntity(userInfoDto);
            UserInfo userInfoSaved = userInfoRepository.findByUuid(userInfoDto.getUuid());
            if (userInfoSaved != null && !userInfoSaved.equals(userInfo)) {
                userInfo.setId(userInfoSaved.getId());
                userInfoRepository.save(userInfo);
            }
            if (userInfoSaved == null) {
                userInfoRepository.save(userInfo);
            }
        } catch (IOException ex) {
            throw new MappingPayLoadException("Mapping exception payload: " + userInfoDto.getPayload(), ex);
        }
    }

    @Override
    public Optional<UserInfoDto> findSynchData(final UUID uuid) {
        UserInfo userInfo = userInfoRepository.findByUuid(uuid);
        if (userInfo == null) return Optional.empty();
        return Optional.of(entityDtoMapper.convertUserInfoToDto(userInfo));
    }

    @Override
    public void saveActivityData(final ActivityUserInfoDto activityUserInfoDto) {
        ActivityUserInfo activityUserInfo = entityDtoMapper.convertActivityUserInfoDtoToEntity(activityUserInfoDto);
        activityUserInfoRepository.save(activityUserInfo);
    }
}
