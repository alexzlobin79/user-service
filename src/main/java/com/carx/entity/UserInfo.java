package com.carx.entity;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Entity
@Table(name = "USER_INFO")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class UserInfo extends BaseEntity {
    @Column(name = "UUID", unique = true, nullable = false, columnDefinition = "uuid")
    @NotNull
    private UUID uuid;
    @Column(name = "MONEY", nullable = false)
    @NotNull
    private Integer money;
    @Column(name = "COUNTRY", nullable = false)
    @NotNull
    private String country;
    @Column(name = "PAYLOAD", nullable = false, columnDefinition = "jsonb")
    @NotNull
    @Type(type = "jsonb-node")
    private JsonNode payload;
}