package com.carx.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "ACTIVITY_USER_INFO")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ActivityUserInfo extends BaseEntity {
    @Column(name = "UUID", unique = false, nullable = false, columnDefinition = "uuid")
    @NotNull
    private UUID uuid;
    @Column(name = "ACTIVITY", nullable = false)
    @NotNull
    private Integer activity;
    @NotNull
    private LocalDateTime dateTime;
}
