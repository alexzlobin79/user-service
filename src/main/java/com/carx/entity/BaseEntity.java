package com.carx.entity;

import com.vladmihalcea.hibernate.type.json.JsonNodeBinaryType;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;

@TypeDefs({@TypeDef(name = "jsonb-node", typeClass = JsonNodeBinaryType.class)})
@MappedSuperclass
@Data
@NoArgsConstructor
public abstract class BaseEntity {
	@Column(name = "ID", updatable = false, nullable = false, insertable = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Id
	private Long Id;
}

