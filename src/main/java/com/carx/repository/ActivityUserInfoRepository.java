package com.carx.repository;

import com.carx.entity.ActivityUserInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ActivityUserInfoRepository extends JpaRepository<ActivityUserInfo, Long> {
    ActivityUserInfo findByUuid(UUID uuid);
}
