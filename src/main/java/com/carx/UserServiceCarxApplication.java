package com.carx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserServiceCarxApplication {

	public static void main(String[] args){
		SpringApplication.run(UserServiceCarxApplication.class, args);
	}
}
