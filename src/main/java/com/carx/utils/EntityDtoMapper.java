package com.carx.utils;

import com.carx.dto.common.UserInfoDto;
import com.carx.dto.request.ActivityUserInfoDto;
import com.carx.entity.ActivityUserInfo;
import com.carx.entity.UserInfo;
import com.carx.exception.mapping.MappingPayLoadException;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDateTime;

@Component
public class EntityDtoMapper {
    private final static ObjectMapper mapper = new ObjectMapper();
    private static final String PROPERTY_MONEY = "money";
    private static final String PROPERTY_COUNTRY = "country";

    public UserInfoDto convertUserInfoToDto(final UserInfo userInfo) {
        UserInfoDto userInfoDto = new UserInfoDto();
        userInfoDto.setUuid(userInfo.getUuid());
        userInfoDto.setPayload(userInfo.getPayload());
        return userInfoDto;
    }

    public UserInfo convertUserInfoDtoToEntity(final UserInfoDto userInfoDto) throws IOException {
        UserInfo userInfo = new UserInfo();
        userInfo.setUuid(userInfoDto.getUuid());
        userInfo.setPayload(userInfoDto.getPayload());
        enrichUserInfo(userInfo, userInfoDto);
        return userInfo;
    }

    public ActivityUserInfo convertActivityUserInfoDtoToEntity(final ActivityUserInfoDto activityUserInfoDto) {
        ActivityUserInfo activityUserInfo = new ActivityUserInfo();
        activityUserInfo.setUuid(activityUserInfoDto.getUuid());
        activityUserInfo.setActivity(activityUserInfoDto.getActivity());
        activityUserInfo.setDateTime(LocalDateTime.now());
        return activityUserInfo;
    }

    private void enrichUserInfo(UserInfo userInfo, UserInfoDto userInfoDto) throws IOException, MappingPayLoadException {
        ExtractedProperty extractedProperty = mapper.treeToValue(userInfoDto.getPayload(), ExtractedProperty.class);
        if (StringUtils.isBlank(extractedProperty.getCountry()))
            throw new MappingPayLoadException("property: " + PROPERTY_COUNTRY + " is blank");
        if (extractedProperty.getMoney() == null)
            throw new MappingPayLoadException("property: " + PROPERTY_MONEY + " is null");
        userInfo.setCountry(extractedProperty.getCountry());
        userInfo.setMoney(extractedProperty.getMoney());
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @Getter
    private static class ExtractedProperty {
        private Integer money;
        private String country;
    }
}
