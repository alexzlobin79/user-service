# user-service

Task for CarX-Tech

##Running in development environment

  spring-boot:run

##Build

  maven clean package

##Running in JVM

  java -jar service-user-carx_tech-0.0.1-SNAPSHOT.jar -Dspring.profiles.active=swagger,prod

##Profiles

swagger-enable swagger

dev - Developing

prod -Production

test - Test

##Unit Test(only for Unit Test)

  mvn test
  
##Integration Test

  running in development environment
  
##Actuator

  Application has Actuator
  
  
  










